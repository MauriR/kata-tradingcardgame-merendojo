describe('TradingCardGame', () => {

  it('gives a player a mana slot when they are activated', () => {
    const game = new TradingCardGame()

    game.activatePlayer()

    const currentPlayer = game.getActivePlayer()
    expect(currentPlayer.manaSlots()).toBe(1)
  })

  it('gives a player a mana slot when they are activated 2', () => {
    const game = new TradingCardGame()

    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()

    const currentPlayer = game.getActivePlayer()
    expect(currentPlayer.manaSlots()).toBe(2)
  })

  it('stablishes a specific number of maximum mana slots', () => {
    const game = new TradingCardGame()

    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()
    game.activatePlayer()

    const currentPlayer = game.getActivePlayer()
    expect(currentPlayer.manaSlots()).toBe(10)
  })

  

})

function fakeMethodInMasterBranch(){
  console.log('This change was introduced in the second set of changes');
}
function addSomeChangesToPractiseRebase(){
  console.log("Second commit with changes to practise rebase");
}

function addThirdSetOfChanges(){
  console.log('Third commit with changes');
}
