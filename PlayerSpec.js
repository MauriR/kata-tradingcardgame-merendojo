describe('Player', () => {
    it('starts game with 0 mana slots', () => {
        
        const player = new Player()

        expect (player.manaSlots()).toBe(0)
    })
})