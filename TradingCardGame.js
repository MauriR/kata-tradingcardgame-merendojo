class TradingCardGame {
    constructor() {
      this.playerOne = new Player()
      this.currentTurn = 1
      this.masterBranchDifference = 'Difference created in the second set of changes at master branch'
      this.branchDifference = "Another difference created to practise rebase"
      this.branchDifferenceSecondCommit = "Another difference in another commit"
    }
  
    activatePlayer() {
      console.log('This console log was added through practise-rebase branch with learning purposes');
      if (this._isPlayerOneTurn()) {
        this.playerOne.active()
      }
      this.currentTurn++
    }
  
    getActivePlayer() {
      console.log("Second commit with differences");
      const currentPlayer = this.playerOne
      const secondCommit = "Another difference"
      return currentPlayer
    }
  
    _isPlayerOneTurn() {
      console.log("practise rebase 4th set of changes, just this commit");
      return (this.currentTurn % 2 !== 0)
    }
  }